package com.kopok.blog.config;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Blog.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    private final MysqlProperties blog = new MysqlProperties();

    private class MysqlProperties{
        private final DataSourceProperties datasource = new DataSourceProperties();
        private final JpaProperties jpa = new JpaProperties();
        
        public DataSourceProperties getDatasource() {
            return datasource;
        }

        public JpaProperties getJpa() {
            return jpa;
        }
    }

    public MysqlProperties getBlog() {
        return blog;
    }
}
