/**
 * View Models used by Spring MVC REST controllers.
 */
package com.kopok.blog.web.rest.vm;
